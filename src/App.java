import com.devcamp.shapes.Circle;
import com.devcamp.shapes.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green");
        //System.out.println(circle1);
        //System.out.println(circle2);
        //System.out.println(circle3);

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);

        System.out.println(cylinder1.toString());
        System.out.println(cylinder2.toString());

        System.out.println(cylinder1.getVolumn());
        System.out.println(cylinder2.getVolumn());
        System.out.println(cylinder3.getVolumn());
        System.out.println(cylinder4.getVolumn());
    }
}
